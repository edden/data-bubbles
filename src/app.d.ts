// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
declare global {
	namespace App {
		// interface Error {}
		// interface Locals {}
		// interface PageData {}
		// interface Platform {}
	}
}

interface ItemInterface {
  quant: string | number
  text: string
  subItems?: ItemInterface[]
}

export {
  ItemInterface
};
